package com.yuri.googlesearchexample.network;

import com.yuri.googlesearchexample.network.model.Container;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Created by Юрий on 25.09.2015.
 */
public interface NetApi {

    @GET("/images?v=1.0")
    public void getResult(
            @Query("start") String start,
            @Query("rsz") String size,
            @Query("q") String search,
            Callback<Container> curatorCallback);
}