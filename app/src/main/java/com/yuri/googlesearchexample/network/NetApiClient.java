package com.yuri.googlesearchexample.network;

import android.content.Context;

import com.google.gson.Gson;
import com.squareup.okhttp.OkHttpClient;

import java.util.concurrent.TimeUnit;

import retrofit.RestAdapter;
import retrofit.client.OkClient;
import retrofit.converter.GsonConverter;

public class NetApiClient {

    private static final String ENDPOINT = "https://ajax.googleapis.com/ajax/services/search/";
    private static NetApi netApi;

    public static NetApi getInstance(Context context) {
        if (netApi==null){
            OkHttpClient okHttpClient = new OkHttpClient();
            okHttpClient.setFollowRedirects(true);
            okHttpClient.setReadTimeout(5, TimeUnit.SECONDS);
            okHttpClient.setConnectTimeout(5, TimeUnit.SECONDS);
            okHttpClient.setWriteTimeout(5, TimeUnit.SECONDS);


            okHttpClient.setFollowRedirects(false);
            RestAdapter restAdapter = new RestAdapter.Builder()
                    .setClient(new OkClient(okHttpClient))
                    .setErrorHandler(new RetrofitErrorHandler(context))
                    .setEndpoint(ENDPOINT)
                    .setLogLevel(RestAdapter.LogLevel.FULL)
                    .setConverter(new GsonConverter(new Gson()))
                    .build();
            netApi = restAdapter.create(NetApi.class);
        }
        return netApi;
    }


}
