package com.yuri.googlesearchexample.network;

public class RetrofitExeption extends Exception{

    public RetrofitExeption (String message){
        super(message);
    }
}
