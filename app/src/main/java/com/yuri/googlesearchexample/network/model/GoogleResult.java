package com.yuri.googlesearchexample.network.model;

/**
 * Created by Юрий on 25.09.2015.
 */
public class GoogleResult {
    private String unescapedUrl;
    private String url;
    private String title;
    private String titleNoFormatting;

    public String getUnescapedUrl() {
        return unescapedUrl;
    }

    public void setUnescapedUrl(String unescapedUrl) {
        this.unescapedUrl = unescapedUrl;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitleNoFormatting() {
        return titleNoFormatting;
    }

    public void setTitleNoFormatting(String titleNoFormatting) {
        this.titleNoFormatting = titleNoFormatting;
    }

    @Override
    public String toString() {
        return "GoogleResult{" +
                "unescapedUrl='" + unescapedUrl + '\'' +
                ", url='" + url + '\'' +
                ", title='" + title + '\'' +
                ", titleNoFormatting='" + titleNoFormatting + '\'' +
                '}';
    }
}
