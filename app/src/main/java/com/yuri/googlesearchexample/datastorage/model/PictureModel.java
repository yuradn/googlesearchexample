package com.yuri.googlesearchexample.datastorage.model;

import java.io.Serializable;

public class PictureModel implements Serializable {

    //private Long id;
    private String url;
    private String title;
    private boolean check = false;

    public PictureModel() {}

    public PictureModel(Long id, String url, String title){
        //this.id    = id;
        this.url   = url;
        this.title = title;
    }

    /*public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }*/

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isCheck() {
        return check;
    }

    public void setCheck(boolean check) {
        this.check = check;
    }

    @Override
    public String toString() {
        return "PictureModel{" +
                ", url='" + url + '\'' +
                ", title='" + title + '\'' +
                '}';
    }
}
