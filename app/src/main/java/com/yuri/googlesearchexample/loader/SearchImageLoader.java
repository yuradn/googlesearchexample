package com.yuri.googlesearchexample.loader;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.content.Loader;
import android.util.Log;

import com.yuri.googlesearchexample.R;
import com.yuri.googlesearchexample.network.model.Container;
import com.yuri.googlesearchexample.network.model.GoogleResult;
import com.yuri.googlesearchexample.network.NetApiClient;
import com.yuri.googlesearchexample.datastorage.model.PictureModel;
import com.yuri.googlesearchexample.utils.Constants;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Юрий on 27.09.2015.
 */
public class SearchImageLoader extends Loader<Object> {
    private final static String TAG = SearchImageLoader.class.getSimpleName();
    private int indexImage = 0;
    private boolean isError=false;
    private String search = "";
    private Context context;

    public SearchImageLoader(Context context, Bundle args) {
        super(context);
        this.context = context;
        if (args!=null) {
            search = args.getString(Constants.SEARCH_STRING);
            indexImage = args.getInt(Constants.INDEX_PICTURES, 0);
        }
    }

    @Override
    protected void onForceLoad() {
        super.onForceLoad();
        List<PictureModel> pictureModels = new ArrayList<>();
        sendRequest(pictureModels);
    }

    private void sendRequest(List<PictureModel> pictureModels) {
        isError=false;
        parse(pictureModels, "" + indexImage, "5", search);
        parse(pictureModels, "" + indexImage, "5", search);
    }

    @Override
    protected void onReset() {
        super.onReset();
        indexImage = 0;
    }

    private void parse(List<PictureModel> pictureModels,
            String index, String range, String search){
        List<PictureModel> tmpList = new ArrayList<>();
        NetApiClient.getInstance(context).getResult(index, range, search, new Callback<Container>() {
            @Override
            public void success(Container container, Response response) {
                if (container.getResponseData().getResults().size()<5) {
                    sendNotFound();
                    return;
                }
                for (GoogleResult result : container.getResponseData().getResults()) {
                    PictureModel model = new PictureModel();
                    model.setCheck(false);
                    model.setTitle(result.getTitle());
                    model.setUrl(result.getUrl());
                    tmpList.add(model);
                    Log.d(TAG, model.toString());
                }
                addToModelList(pictureModels, tmpList);
            }

            @Override
            public void failure(RetrofitError error) {
                error.printStackTrace();
                sendError(error);
            }


        });
    }

    private synchronized void sendError(RetrofitError error){
        if (isError) return;
        isError = true;
        deliverResult(error);
    }

    private synchronized void sendNotFound(){
        if (isError) return;
        isError=true;
        String errorMessage = context.getString(R.string.request_by_word)
                + search + " " +context.getString(R.string.not_found);
        deliverResult(errorMessage);
    }

    private synchronized void addToModelList(List<PictureModel> pictureModels,
                                             List<PictureModel> tmpModel){
        if (isError) return;
        pictureModels.addAll(tmpModel);
        if (pictureModels.size()>=10) returnResult(pictureModels);
    }

    private void returnResult(List<PictureModel> pictureModels) {
        indexImage+=pictureModels.size();
        deliverResult(pictureModels);
    }

    @Override
    protected void onStartLoading() {
        super.onStartLoading();
        Log.d(TAG, hashCode() + " onStartLoading");
    }

    @Override
    protected void onStopLoading() {
        super.onStopLoading();
        Log.d(TAG, hashCode() + " onStopLoading");
    }

    @Override
    protected void onAbandon() {
        super.onAbandon();
        Log.d(TAG, hashCode() + " onAbandon");
    }

}
