package com.yuri.googlesearchexample.chapters.fragment;

import android.content.ContentValues;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.util.Pair;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;

import com.yuri.googlesearchexample.R;
import com.yuri.googlesearchexample.chapters.base.BaseFragment;
import com.yuri.googlesearchexample.datastorage.model.PictureModel;
import com.yuri.googlesearchexample.datastorage.provider.SavedPicturesProvider;
import com.yuri.googlesearchexample.loader.SearchImageLoader;
import com.yuri.googlesearchexample.ui.adapters.SearchAdapter;
import com.yuri.googlesearchexample.utils.Constants;

import java.util.List;

import butterknife.Bind;
import retrofit.RetrofitError;

public class SearchPictureFragment extends BaseFragment
        implements LoaderManager.LoaderCallbacks<Object>,
                   SearchAdapter.MyAdapterEventListener{
    private final static String TAG = SearchPictureFragment.class.getSimpleName();
    private SearchAdapter searchAdapter;
    private boolean isRequest=false;
    private Snackbar snackbar;
    private String search;
    private boolean errorCount=false;

    @Bind(R.id.listContainer)
    protected FrameLayout container;
    @Bind(R.id.edSearch)
    protected EditText edSearch;
    @Bind(R.id.recyclerView)
    protected RecyclerView recyclerView;

    @Override
    protected int setLayoutRes() {
        return R.layout.fragment_search;
    }

    @Override
    protected void afterCreateView() {
        setRetainInstance(true);
        initRecyclerView();
        initSearchText();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        closeKeyboard();
    }

    private void initSearchText() {
        edSearch.setOnKeyListener((v, keyCode, event) -> {
            if (keyCode == EditorInfo.IME_ACTION_SEARCH ||
                    keyCode == EditorInfo.IME_ACTION_DONE ||
                    event.getAction() == KeyEvent.ACTION_DOWN &&
                            event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
                closeKeyboard();
                String word = edSearch.getText().toString();
                if (TextUtils.isEmpty(word)) return false;
                if (word.equals(search) && !errorCount) return false;
                search = word;
                searchAdapter = null;
                initLoader(search, 0);
                nextSearchIteration();
            }
            return false;
        });
    }

    @Override public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        reInitMyAdapter();
        reInitMyLoader();

    }

    private void reInitMyLoader() {
        if (!TextUtils.isEmpty(search)) initLoader(search, searchAdapter.getPictureModels().size());
    }

    private void reInitMyAdapter() {
        if (searchAdapter!=null) {
            searchAdapter = new SearchAdapter(getContext(), searchAdapter.getPictureModels());
            searchAdapter.setMyAdapterEventListener(this);
            recyclerView.setAdapter(searchAdapter);
        }
    }

    //TODO remember keyboard state
    private void closeKeyboard() {
        getActivity().getWindow()
                .setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        View view = getActivity().getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)
                    getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @Override
    public void onClickImage(String url) {
        PreviewImageFragment viewImageFragment = new PreviewImageFragment();
        Bundle args = new Bundle();
        args.putString(Constants.IMAGE_URL, url);
        viewImageFragment.setArguments(args);
        getFragmentManager().beginTransaction()
                .add(R.id.parentContainer, viewImageFragment,
                        PreviewImageFragment.class.getCanonicalName())
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onCheckoBoxChange(boolean check, int position) {
        ContentValues cv = new ContentValues();
        PictureModel model = searchAdapter.getPictureModels().get(position);
        cv.put(SavedPicturesProvider.ITEM_TITLE, model.getTitle());
        cv.put(SavedPicturesProvider.ITEM_LINK, model.getUrl());
        getContext().getContentResolver()
                .insert(SavedPicturesProvider.ITEM_CONTENT_URI, cv);
    }

    @Override
    public void onEndList(int size) {
        if (size>=Constants.MAX_INDEX||isRequest) return;
        nextSearchIteration();
    }

    private void initRecyclerView() {
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(mLayoutManager);
    }

    @Override
    public Loader<Object> onCreateLoader(int id, Bundle args) {
        Loader<Object> loader = null;
        if (id == Constants.LOADER_SEARCH_ID) {
            loader = new SearchImageLoader(getContext(), args);
        }
        return loader;
    }

    @Override
    public void onLoadFinished(Loader<Object> loader, Object result) {
        isRequest=false;
        dismissSnackbar();
        if (processingError(result)) return;
        if (processingNotFoundError(result)) return;
        processingResult((List<PictureModel>) result);
    }

    private boolean processingNotFoundError(Object result) {
        if (result.getClass().equals(String.class)) {
            errorCount = true;
            showErrorDialog((String)result);
            recyclerView.setAdapter(null);
            recyclerView.removeAllViews();
            searchAdapter=null;
            return true;
        }
        return false;
    }

    private void processingResult(List<PictureModel> result) {
        errorCount=false;
        try {
            List<PictureModel> pictureModels = result;
            for (int i = 0; i < pictureModels.size(); i++) {
                Log.d(TAG, pictureModels.get(i).toString());
            }
            if (searchAdapter == null) {
                searchAdapter = new SearchAdapter(getContext(), pictureModels);
                recyclerView.setAdapter(searchAdapter);
                searchAdapter.setMyAdapterEventListener(this);
            } else {
                searchAdapter.getPictureModels().addAll(pictureModels);
                searchAdapter.notifyItemRangeInserted(searchAdapter.getPictureModels().size() - 10, 10);
                searchAdapter.notifyItemRangeChanged(searchAdapter.getPictureModels().size() - 10, 10);
            }
        } catch (ClassCastException e) {
            e.printStackTrace();
        }
    }

    private void dismissSnackbar() {
        if (snackbar != null) {
            if (snackbar.isShown()) snackbar.dismiss();
        }
    }

    private boolean processingError(Object result) {
        if (result.getClass().equals(RetrofitError.class)) {
            errorCount = true;
            RetrofitError error;
            String errorMessage;
            try {
                error = (RetrofitError) result;
                errorMessage = error.getMessage();
            } catch (ClassCastException e) {
                e.printStackTrace();
                errorMessage = getString(R.string.unexpected_error);
            }
            showErrorDialog(errorMessage);
            return true;
            }
        return false;
    }

    private void showErrorDialog(String errorMessage) {
        new AlertDialog.Builder(getContext())
                .setTitle(getString(R.string.error))
                .setCancelable(true)
                .setNegativeButton(getString(R.string.ok), (dialog, which) -> {
                    dialog.dismiss();
                })
                .setMessage(errorMessage)
                .setIcon(R.drawable.error)
                .show();
    }

    @Override
    public void onLoaderReset(Loader<Object> loader) {}

    private void initLoader(String search, int index) {
        Loader<Pair> loader = getLoaderManager().getLoader(Constants.LOADER_SEARCH_ID);
        Bundle bndl = new Bundle();
        bndl.putString(Constants.SEARCH_STRING, search);
        bndl.putInt(Constants.INDEX_PICTURES, index);
        if (loader != null) {
            getLoaderManager().restartLoader(Constants.LOADER_SEARCH_ID, bndl, this);
        }
        if (loader == null) {
            getLoaderManager().initLoader(Constants.LOADER_SEARCH_ID, bndl, this);
        }
    }

    private void nextSearchIteration() {
        isRequest=true;
        initSnackBar();
        Loader<Pair> loader;
        loader = getLoaderManager().getLoader(Constants.LOADER_SEARCH_ID);
        loader.forceLoad();
    }

    private void initSnackBar() {
        snackbar = Snackbar.make(container, R.string.loading_info,
                Snackbar.LENGTH_INDEFINITE)
                .setAction(R.string.done, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        snackbar.dismiss();
                    }
                })
                .setActionTextColor(Color.RED);
        snackbar.show();
    }
}
