package com.yuri.googlesearchexample.chapters.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;

public class MemoryFragment extends Fragment {
    private PagerAdapter mPagerAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    public PagerAdapter getmPagerAdapter() {
        return mPagerAdapter;
    }

    public void setmPagerAdapter(PagerAdapter mPagerAdapter) {
        this.mPagerAdapter = mPagerAdapter;
    }

}
