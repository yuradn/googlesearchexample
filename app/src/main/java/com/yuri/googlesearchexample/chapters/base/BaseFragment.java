package com.yuri.googlesearchexample.chapters.base;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;

/**
 * Created by Юрий on 22.09.2015.
 */
public abstract class BaseFragment extends Fragment {

    protected View rootView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(setLayoutRes(), container, false);
        ButterKnife.bind(this, rootView);

        afterCreateView();
        return rootView;
    }

    protected abstract int setLayoutRes();

    protected abstract void afterCreateView();

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(rootView);
    }
}