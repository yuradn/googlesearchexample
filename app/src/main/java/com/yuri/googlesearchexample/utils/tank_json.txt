{
 "kind": "customsearch#search",
 "url": {
  "type": "application/json",
  "template": "https://www.googleapis.com/customsearch/v1?q={searchTerms}&num={count?}&start={startIndex?}&lr={language?}&safe={safe?}&cx={cx?}&cref={cref?}&sort={sort?}&filter={filter?}&gl={gl?}&cr={cr?}&googlehost={googleHost?}&c2coff={disableCnTwTranslation?}&hq={hq?}&hl={hl?}&siteSearch={siteSearch?}&siteSearchFilter={siteSearchFilter?}&exactTerms={exactTerms?}&excludeTerms={excludeTerms?}&linkSite={linkSite?}&orTerms={orTerms?}&relatedSite={relatedSite?}&dateRestrict={dateRestrict?}&lowRange={lowRange?}&highRange={highRange?}&searchType={searchType}&fileType={fileType?}&rights={rights?}&imgSize={imgSize?}&imgType={imgType?}&imgColorType={imgColorType?}&imgDominantColor={imgDominantColor?}&alt=json"
 },
 "queries": {
  "request": [
   {
    "title": "Google Custom Search - танк",
    "totalResults": "9",
    "searchTerms": "танк",
    "count": 9,
    "startIndex": 1,
    "inputEncoding": "utf8",
    "outputEncoding": "utf8",
    "safe": "off",
    "cx": "006362779814540710079:vbyugayezxk",
    "fileType": "png"
   }
  ]
 },
 "context": {
  "title": "MySearch"
 },
 "searchInformation": {
  "searchTime": 0.970233,
  "formattedSearchTime": "0.97",
  "totalResults": "9",
  "formattedTotalResults": "9"
 },
 "items": [
  {
   "kind": "customsearch#result",
   "title": "Файл:Танк в L4D.png — Zombie вики",
   "htmlTitle": "Файл:\u003cb\u003eТанк\u003c/b\u003e в L4D.png — Zombie вики",
   "link": "http://ru.zombie.wikia.com/wiki/%D0%A4%D0%B0%D0%B9%D0%BB:%D0%A2%D0%B0%D0%BD%D0%BA_%D0%B2_L4D.png",
   "displayLink": "ru.zombie.wikia.com",
   "snippet": "Громила. Громила (танк) — вид Б.О.О. или зомби, который является особым \nзараженным. Отличается от остальных… См. полный список \u003e ...",
   "htmlSnippet": "Громила. Громила (\u003cb\u003eтанк\u003c/b\u003e) — вид Б.О.О. или зомби, который является особым \u003cbr\u003e\nзараженным. Отличается от остальных… См. полный список &gt;&nbsp;...",
   "cacheId": "RFeyaBDjjZ8J",
   "formattedUrl": "ru.zombie.wikia.com/wiki/Файл:Танк_в_L4D.png",
   "htmlFormattedUrl": "ru.zombie.wikia.com/wiki/Файл:\u003cb\u003eТанк\u003c/b\u003e_в_L4D.png",
   "pagemap": {
    "cse_image": [
     {
      "src": "http://vignette3.wikia.nocookie.net/zombie/images/5/5f/%D0%A2%D0%B0%D0%BD%D0%BA_%D0%B2_L4D.png/revision/latest?cb=20150228100034&path-prefix=ru"
     }
    ],
    "cse_thumbnail": [
     {
      "width": "239",
      "height": "211",
      "src": "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTq8CmvncoHVzbt_XvjPe8LmRyLMsnNUazsGwDVyteLBU9BAE_1XZmIcYc"
     }
    ],
    "metatags": [
     {
      "viewport": "width=device-width, initial-scale=1.0, user-scalable=yes",
      "fb:app_id": "112328095453510",
      "og:type": "article",
      "og:site_name": "Zombie вики",
      "og:title": "Танк в L4D.png",
      "og:url": "http://ru.zombie.wikia.com/wiki/%D0%A4%D0%B0%D0%B9%D0%BB:%D0%A2%D0%B0%D0%BD%D0%BA_%D0%B2_L4D.png",
      "og:image": "http://vignette3.wikia.nocookie.net/zombie/images/5/5f/%D0%A2%D0%B0%D0%BD%D0%BA_%D0%B2_L4D.png/revision/latest?cb=20150228100034&path-prefix=ru",
      "apple-itunes-app": "app-id=623705389"
     }
    ]
   }
  },
  {
   "kind": "customsearch#result",
   "title": "Файл:Танк (Dead Trigger 2).png — Вікіпедія",
   "htmlTitle": "Файл:\u003cb\u003eТанк\u003c/b\u003e (Dead Trigger 2).png — Вікіпедія",
   "link": "https://uk.wikipedia.org/wiki/%D0%A4%D0%B0%D0%B9%D0%BB:%D0%A2%D0%B0%D0%BD%D0%BA_(Dead_Trigger_2).png",
   "displayLink": "uk.wikipedia.org",
   "snippet": "Назва, Танк (Dead Trigger 2).png. Опис, Бос із гри Dead Trigger 2. Джерело, \nhttp://www.madfingergames.com/deadtrigger2/knowledgebase/zombies.",
   "htmlSnippet": "Назва, \u003cb\u003eТанк\u003c/b\u003e (Dead Trigger 2).png. Опис, Бос із гри Dead Trigger 2. Джерело, \u003cbr\u003e\nhttp://www.madfingergames.com/deadtrigger2/knowledgebase/zombies.",
   "cacheId": "IRtFWwiNKoUJ",
   "formattedUrl": "https://uk.wikipedia.org/wiki/Файл:Танк_(Dead_Trigger_2).png",
   "htmlFormattedUrl": "https://uk.wikipedia.org/wiki/Файл:\u003cb\u003eТанк\u003c/b\u003e_(Dead_Trigger_2).png",
   "pagemap": {
    "cse_image": [
     {
      "src": "https://upload.wikimedia.org/wikipedia/uk/2/29/%D0%A2%D0%B0%D0%BD%D0%BA_(Dead_Trigger_2).png"
     }
    ],
    "cse_thumbnail": [
     {
      "width": "176",
      "height": "286",
      "src": "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTTFiog_Hv3uqDCKpdru7Q4XCUTIIuqX_yqHYJy6lgQ4pCEU-jpkzdyuUU"
     }
    ]
   }
  },
  {
   "kind": "customsearch#result",
   "title": "Файл:Warhammer 40k Stormhammer.png — Википедия",
   "htmlTitle": "Файл:Warhammer 40k Stormhammer.png — Википедия",
   "link": "https://ru.wikipedia.org/wiki/%D0%A4%D0%B0%D0%B9%D0%BB:Warhammer_40k_Stormhammer.png",
   "displayLink": "ru.wikipedia.org",
   "snippet": "Танк Гибельный Клинок · Цель использования, иллюстрация внешнего вида \nочень специфической модификации танка «Карающий Меч» ...",
   "htmlSnippet": "\u003cb\u003eТанк\u003c/b\u003e Гибельный Клинок &middot; Цель использования, иллюстрация внешнего вида \u003cbr\u003e\nочень специфической модификации \u003cb\u003eтанка\u003c/b\u003e «Карающий Меч»&nbsp;...",
   "cacheId": "qGkIPuYOswkJ",
   "formattedUrl": "https://ru.wikipedia.org/.../Файл:Warhammer_40k_Stormhammer.png",
   "htmlFormattedUrl": "https://ru.wikipedia.org/.../Файл:Warhammer_40k_Stormhammer.png",
   "pagemap": {
    "cse_image": [
     {
      "src": "https://upload.wikimedia.org/wikipedia/ru/1/1d/Warhammer_40k_Stormhammer.png"
     }
    ],
    "cse_thumbnail": [
     {
      "width": "144",
      "height": "102",
      "src": "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTDYBYMZYC7GskYBuepDAVqZ52cWBDFVhjmujW3pTB4gI0sesrI7wEdcfA"
     }
    ]
   }
  },
  {
   "kind": "customsearch#result",
   "title": "File:Tank-Destroyer-Branch-Insig.png - Wikimedia Commons",
   "htmlTitle": "File:Tank-Destroyer-Branch-Insig.png - Wikimedia Commons",
   "link": "https://commons.wikimedia.org/wiki/File:Tank-Destroyer-Branch-Insig.png",
   "displayLink": "commons.wikimedia.org",
   "snippet": "Jun 27, 2011 ... ... мировая война/Шаблоны по военной технике · Стюарт (танк) · M2 (лёгкий \nтанк) · M22 Локаст · Проект:Бронетехника/Шаблоны ...",
   "htmlSnippet": "Jun 27, 2011 \u003cb\u003e...\u003c/b\u003e ... мировая война/Шаблоны по военной технике &middot; Стюарт (\u003cb\u003eтанк\u003c/b\u003e) &middot; M2 (лёгкий \u003cbr\u003e\n\u003cb\u003eтанк\u003c/b\u003e) &middot; M22 Локаст &middot; Проект:Бронетехника/Шаблоны&nbsp;...",
   "cacheId": "5ELpnRWlyGQJ",
   "formattedUrl": "https://commons.wikimedia.org/.../File:Tank-Destroyer-Branch-Insig.png",
   "htmlFormattedUrl": "https://commons.wikimedia.org/.../File:Tank-Destroyer-Branch-Insig.png",
   "pagemap": {
    "cse_image": [
     {
      "src": "https://upload.wikimedia.org/wikipedia/commons/9/93/Tank-Destroyer-Branch-Insig.png"
     }
    ],
    "cse_thumbnail": [
     {
      "width": "333",
      "height": "151",
      "src": "https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcTWYvuacq58yWaEbyGa8rygxry91LInk9gm2vLch_hHL1Q-8OAnVcNGtZ0"
     }
    ],
    "hproduct": [
     {
      "fn": "Tank-Destroyer-Branch-Insig.png"
     }
    ]
   }
  },
  {
   "kind": "customsearch#result",
   "title": "Файл:Танки на станции Лозовая по пути на фронт 1919 год.PNG ...",
   "htmlTitle": "Файл:\u003cb\u003eТанки\u003c/b\u003e на станции Лозовая по пути на фронт 1919 год.PNG \u003cb\u003e...\u003c/b\u003e",
   "link": "https://ru.wikipedia.org/wiki/%D0%A4%D0%B0%D0%B9%D0%BB:%D0%A2%D0%B0%D0%BD%D0%BA%D0%B8_%D0%BD%D0%B0_%D1%81%D1%82%D0%B0%D0%BD%D1%86%D0%B8%D0%B8_%D0%9B%D0%BE%D0%B7%D0%BE%D0%B2%D0%B0%D1%8F_%D0%BF%D0%BE_%D0%BF%D1%83%D1%82%D0%B8_%D0%BD%D0%B0_%D1%84%D1%80%D0%BE%D0%BD%D1%82_1919_%D0%B3%D0%BE%D0%B4.PNG",
   "displayLink": "ru.wikipedia.org",
   "snippet": "А. Трембовельский. 3-й отряд танков [командир полк. Миронович] под \nЦарицыном. // Вооружённые силы на Юге России. Январь-июнь 1919 года. / \nд.и.н.",
   "htmlSnippet": "А. Трембовельский. 3-й отряд \u003cb\u003eтанков\u003c/b\u003e [командир полк. Миронович] под \u003cbr\u003e\nЦарицыном. // Вооружённые силы на Юге России. Январь-июнь 1919 года. / \u003cbr\u003e\nд.и.н.",
   "cacheId": "vO9aW70m9ZwJ",
   "formattedUrl": "https://ru.wikipedia.org/.../Файл:Танки_на_станции_Лозовая_по_пути_на_ фронт_1919_год.PNG",
   "htmlFormattedUrl": "https://ru.wikipedia.org/.../Файл:\u003cb\u003eТанки\u003c/b\u003e_на_станции_Лозовая_по_пути_на_ фронт_1919_год.PNG",
   "pagemap": {
    "cse_image": [
     {
      "src": "https://upload.wikimedia.org/wikipedia/ru/4/4a/%D0%A2%D0%B0%D0%BD%D0%BA%D0%B8_%D0%BD%D0%B0_%D1%81%D1%82%D0%B0%D0%BD%D1%86%D0%B8%D0%B8_%D0%9B%D0%BE%D0%B7%D0%BE%D0%B2%D0%B0%D1%8F_%D0%BF%D0%BE_%D0%BF%D1%83%D1%82%D0%B8_%D0%BD%D0%B0_%D1%84%D1%80%D0%BE%D0%BD%D1%82_1919_%D0%B3%D0%BE%D0%B4.PNG"
     }
    ],
    "cse_thumbnail": [
     {
      "width": "275",
      "height": "183",
      "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcSeA6-dTAWBlH3nDuQJYaXWQao1go3tKLlJ2ClpQ3jBDBeG_P_smINTQvFG"
     }
    ]
   }
  },
  {
   "kind": "customsearch#result",
   "title": "File:Tank Schema2.png - Wikimedia Commons",
   "htmlTitle": "File:Tank Schema2.png - Wikimedia Commons",
   "link": "https://commons.wikimedia.org/wiki/File:Tank_Schema2.png",
   "displayLink": "commons.wikimedia.org",
   "snippet": "Apr 22, 2015 ... Танк. Usage on ca.wikipedia.org. Tanc. Usage on cs.wikipedia.org. Diskuse: ... \nUsage on ru.wikipedia.org. Танк. Usage on sk.wikipedia.org.",
   "htmlSnippet": "Apr 22, 2015 \u003cb\u003e...\u003c/b\u003e \u003cb\u003eТанк\u003c/b\u003e. Usage on ca.wikipedia.org. Tanc. Usage on cs.wikipedia.org. Diskuse: ... \u003cbr\u003e\nUsage on ru.wikipedia.org. \u003cb\u003eТанк\u003c/b\u003e. Usage on sk.wikipedia.org.",
   "cacheId": "ApJAYuQD0GsJ",
   "formattedUrl": "https://commons.wikimedia.org/wiki/File:Tank_Schema2.png",
   "htmlFormattedUrl": "https://commons.wikimedia.org/wiki/File:Tank_Schema2.png",
   "pagemap": {
    "cse_image": [
     {
      "src": "https://upload.wikimedia.org/wikipedia/commons/b/bb/Tank_Schema2.png"
     }
    ],
    "cse_thumbnail": [
     {
      "width": "259",
      "height": "194",
      "src": "https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcSr0if32ITtpLgtWgX6-0YUOdDyOERhKP6bTzHW3kExGwAzDbaY3MIdPL4O"
     }
    ],
    "hproduct": [
     {
      "fn": "Tank Schema2.png"
     }
    ]
   }
  },
  {
   "kind": "customsearch#result",
   "title": "Файл:Танки Онлайн лого.png — Википедия",
   "htmlTitle": "Файл:\u003cb\u003eТанки\u003c/b\u003e Онлайн лого.png — Википедия",
   "link": "https://ru.wikipedia.org/wiki/%D0%A4%D0%B0%D0%B9%D0%BB:%D0%A2%D0%B0%D0%BD%D0%BA%D0%B8_%D0%9E%D0%BD%D0%BB%D0%B0%D0%B9%D0%BD_%D0%BB%D0%BE%D0%B3%D0%BE.png",
   "displayLink": "ru.wikipedia.org",
   "snippet": "Данный файл является несвободным (не соответствует определению \nсвободного произведения культуры). В соответствии с решением Фонда ...",
   "htmlSnippet": "Данный файл является несвободным (не соответствует определению \u003cbr\u003e\nсвободного произведения культуры). В соответствии с решением Фонда&nbsp;...",
   "cacheId": "gI_NNY2WiS8J",
   "formattedUrl": "https://ru.wikipedia.org/wiki/Файл:Танки_Онлайн_лого.png",
   "htmlFormattedUrl": "https://ru.wikipedia.org/wiki/Файл:\u003cb\u003eТанки\u003c/b\u003e_Онлайн_лого.png",
   "pagemap": {
    "cse_image": [
     {
      "src": "https://upload.wikimedia.org/wikipedia/ru/8/82/%D0%A2%D0%B0%D0%BD%D0%BA%D0%B8_%D0%9E%D0%BD%D0%BB%D0%B0%D0%B9%D0%BD_%D0%BB%D0%BE%D0%B3%D0%BE.png"
     }
    ],
    "cse_thumbnail": [
     {
      "width": "201",
      "height": "250",
      "src": "https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcQUBYkghRWQcMuFz6d-fpwJeWiKvmiWbJ_oD5Dm8Kt62YJZWEbbwJ8OIHA"
     }
    ]
   }
  },
  {
   "kind": "customsearch#result",
   "title": "Файл - 69360801.png-1-.png — Терминатор Wiki",
   "htmlTitle": "Файл - 69360801.png-1-.png — Терминатор Wiki",
   "link": "http://ru.terminator.wikia.com/wiki/%D0%A4%D0%B0%D0%B9%D0%BB:69360801.png-1-.png",
   "displayLink": "ru.terminator.wikia.com",
   "snippet": "Танк; Охотник; T-600. Источник — «http://ru.terminator.wikia.com/wiki/%D0%A4\n%D0%B0%D0%B9%D0%BB:69360801.png-1-.png?oldid=2349». Категории:.",
   "htmlSnippet": "\u003cb\u003eТанк\u003c/b\u003e; Охотник; T-600. Источник — «http://ru.terminator.wikia.com/wiki/%D0%A4\u003cbr\u003e\n%D0%B0%D0%B9%D0%BB:69360801.png-1-.png?oldid=2349». Категории:.",
   "cacheId": "IF8NBshrRksJ",
   "formattedUrl": "ru.terminator.wikia.com/wiki/Файл:69360801.png-1-.png",
   "htmlFormattedUrl": "ru.terminator.wikia.com/wiki/Файл:69360801.png-1-.png",
   "pagemap": {
    "cse_image": [
     {
      "src": "http://img2.wikia.nocookie.net/__cb20130109190830/terminator/ru/images/thumb/0/02/69360801.png-1-.png/500px-69360801.png-1-.png"
     }
    ],
    "cse_thumbnail": [
     {
      "width": "300",
      "height": "168",
      "src": "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQZYRuwkfIDHmJI7719_Q_zaoJH-5WptYzMNa67jva7CkUXh75dg9CZq9dd"
     }
    ],
    "metatags": [
     {
      "viewport": "width=device-width, initial-scale=1.0, user-scalable=yes",
      "fb:app_id": "112328095453510",
      "og:type": "article",
      "og:site_name": "Терминатор Wiki",
      "og:title": "69360801.png-1-.png",
      "og:url": "http://ru.terminator.wikia.com/wiki/%D0%A4%D0%B0%D0%B9%D0%BB:69360801.png-1-.png",
      "og:image": "http://img2.wikia.nocookie.net/__cb20130109190830/terminator/ru/images/thumb/0/02/69360801.png-1-.png/500px-69360801.png-1-.png",
      "apple-itunes-app": "app-id=623705389"
     }
    ]
   }
  },
  {
   "kind": "customsearch#result",
   "title": "Файл:Baneblade.png — Википедия",
   "htmlTitle": "Файл:Baneblade.png — Википедия",
   "link": "https://ru.wikipedia.org/wiki/%D0%A4%D0%B0%D0%B9%D0%BB:Baneblade.png",
   "displayLink": "ru.wikipedia.org",
   "snippet": "Описание, фрагмент скриншота компьютерной игры Dark Crusade, \nзапечатлен танк «Карающий Меч». Источник, Warhammer 40000: Dawn of \nWar - Dark ...",
   "htmlSnippet": "Описание, фрагмент скриншота компьютерной игры Dark Crusade, \u003cbr\u003e\nзапечатлен \u003cb\u003eтанк\u003c/b\u003e «Карающий Меч». Источник, Warhammer 40000: Dawn of \u003cbr\u003e\nWar - Dark&nbsp;...",
   "cacheId": "7gO99-QHffoJ",
   "formattedUrl": "https://ru.wikipedia.org/wiki/Файл:Baneblade.png",
   "htmlFormattedUrl": "https://ru.wikipedia.org/wiki/Файл:Baneblade.png",
   "pagemap": {
    "cse_image": [
     {
      "src": "https://upload.wikimedia.org/wikipedia/ru/6/66/Baneblade.png"
     }
    ],
    "cse_thumbnail": [
     {
      "width": "245",
      "height": "205",
      "src": "https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcQtzoy78OlwYfNscwTEik4hBunz5BhCfUB9niKJIzOuQqJprDfDwTTNw9aM"
     }
    ]
   }
  }
 ]
}