package com.yuri.googlesearchexample.utils;

/**
 * Created by Юрий on 24.09.2015.
 */
public class Constants {
    public static final int LOADER_SEARCH_ID = 1;
    public static final int MAX_INDEX = 60;
    public static final String SEARCH_STRING = "string_search";
    public static final String INDEX_PICTURES = "index_images";
    public static final String IMAGE_URL = "url_image";
}
